

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
   
    [SerializeField] private float speed = 1;
    
    [SerializeField] private GameObject myGameObject;

    [SerializeField] private GameObject Pilar, Pilar2, Stick, Target2,Pivot;

    private Vector3 targetPilar, targeStick;

    

    public bool IsOnPilar, IsOnPilar2 = false;

    public bool  NotOnPilar = false;


    SistemaDeJogo MyState;

    RecyclePilar recy;

    void Start()
    {
        MyState = GameObject.FindGameObjectWithTag("Sistema").GetComponent<SistemaDeJogo>();
        recy = GameObject.FindGameObjectWithTag("Pilar").GetComponent<RecyclePilar>();



    }
    

    void Update()
    {


        
  
    }
        
          
    public void stateChange()
    {
        MyState.state = Sistema.PARTE2;
      

    }
    



     public void  wichtWay()
    {

         
    

            if ( IsOnPilar == true)
            {

            PilarTarget();
         
            }

      
            if (IsOnPilar2 == true)
            {
            PilarTarget2();
            }



            if ( NotOnPilar == true)
            {
            StickTarget();

            }
           

 
     }




    public void wichtWay2()
    {




        if (IsOnPilar == true)
        {

            MovePart2();

        }


        if (IsOnPilar2)
        {
            MovePart();
        
        }



    }





    public void MovePart()
    {


        Vector3 TargetMy = Target2.transform.position;
        TargetMy.y = myGameObject.transform.position.y;



        Vector3 TargetStick = new Vector3( - 25, Pivot.transform.position.y, 0);


        Vector3 TargetPilar2 = Target2.transform.position;
        

        myGameObject.transform.position = Vector2.MoveTowards(myGameObject.transform.position, TargetMy, speed * Time.deltaTime);
        Pilar.transform.position = Vector2.MoveTowards(Pilar.transform.position, TargetPilar2, speed * Time.deltaTime);
        Pivot.transform.position = Vector2.MoveTowards(Pivot.transform.position, TargetStick, speed * Time.deltaTime);


        if (Pilar.transform.position.x == TargetPilar2.x)
        {

            MyState.state = Sistema.PARTE4;


        }



    }





    public void MovePart2()
    {

         Vector3 TargetMy = Target2.transform.position;
         TargetMy.y = myGameObject.transform.position.y;


        Vector3 TargetStick = new Vector3(Pivot.transform.position.y -3, Pivot.transform.position.y, 0);



        Vector3 TargetPilar2 = Target2.transform.position;

        myGameObject.transform.position = Vector2.MoveTowards(myGameObject.transform.position, TargetMy, speed * Time.deltaTime);
        Pilar2.transform.position = Vector2.MoveTowards(Pilar2.transform.position, TargetPilar2, speed * Time.deltaTime);
        Pivot.transform.position = Vector2.MoveTowards(Pivot.transform.position, TargetStick, speed * Time.deltaTime);


        if (Pilar2.transform.position.x == TargetPilar2.x)
        {

            MyState.state = Sistema.PARTE4;


        }



    }



        
    
      public void PilarTarget ()
     {
      //  Debug.Log("Player tocando");
       
        targetPilar = myGameObject.transform.position;
        targetPilar.x = Pilar2.transform.position.x;

        myGameObject.transform.position = Vector2.MoveTowards(myGameObject.transform.position, targetPilar, speed * Time.deltaTime);

        if ( myGameObject.transform.position.x == targetPilar.x && MyState.state == Sistema.PARTE2)
        {
            MyState.state = Sistema.PARTE3;
        }


     }



    public void PilarTarget2()
    {
        Debug.Log("Pilar target 2");


        targetPilar = myGameObject.transform.position;
        targetPilar.x = Pilar.transform.position.x;
      
        myGameObject.transform.position = Vector2.MoveTowards(myGameObject.transform.position, targetPilar, speed * Time.deltaTime);


        if (myGameObject.transform.position.x == targetPilar.x && MyState.state == Sistema.PARTE2)
        {
            MyState.state = Sistema.PARTE3;
        }



    }


    
    

    public void StickTarget ()
     {


        targeStick = myGameObject.transform.position;
        targeStick.x = Stick.transform.position.x ;
        myGameObject.transform.position = Vector2.MoveTowards(myGameObject.transform.position, targeStick, speed * Time.deltaTime);
       

    }






}
